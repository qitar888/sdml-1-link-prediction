#!/bin/bash

display_usage() {
    echo -e "Usage: ./train.sh [task] [training data folder path] [model path] \n"
}

# if less than two / more than four arguments supplied, display usage
if [[ $# -le 2 || $# -ge 4 ]]
then
    display_usage
    exit 1
fi

# check whether user had supplied -h or --help . If yes display usage
if [[ ( $# == "--help") || $# == "-h" ]]
then
    display_usage
    exit 0
fi

######## Important part ########

if [ "$1" = "task2" ]
then
    echo "This is task 2, following arguments are $2, $3"
    exit 0
elif [ "$1" = "task3" ]
then
    echo "This is task 3, following arguments are $2, $3"
    exit 0
else
    echo "Wrong argument, [$1] should be [task2] or [task3]"
    display_usage
    exit 1
fi

######## Important part end ########
