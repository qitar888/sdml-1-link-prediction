import sys
import os
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))  # NOQA


import numpy as np
import torch
import torch.nn as nn

from net import Net
from utils import load_embeddings


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1")
    parser.add_argument(
        '-m', '--model', type=str,
        required=True,
        help="Modle filename"
    )
    parser.add_argument(
        '-te', '--test_edges', type=str,
        required=True,
        help="Training edges filename"
    )
    parser.add_argument(
        '-time', '--time', type=str,
        required=True,
        help="Posix time filename"
    )
    parser.add_argument(
        '-n', '--embeddings', type=str,
        required=True,
        help="Node embeddings filename"
    )
    parser.add_argument(
        '-nt', '--embeddings_type', type=str,
        required=True,
        help="Node embeddings type"
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    timestamp_path = 't3_time_lines_posix.txt'

    test_data = np.loadtxt(args.test_edges, dtype=int)
    test_data = test_data - np.ones(test_data.shape, dtype=int)  # to 0-based
    embeddings = load_embeddings(args.embeddings, args.embeddings_type)
    embeddings = nn.Embedding.from_pretrained(
        torch.FloatTensor(embeddings))
    timestamp = np.loadtxt(args.time, dtype=int)

    def input_features(data):
        return torch.cat((
            embeddings(data).view(data.shape[0], -1),
            torch.FloatTensor(timestamp[data]),
            torch.unsqueeze(
                torch.FloatTensor(
                    timestamp[data[:, 0]] - timestamp[data[:, 1]]
                ), -1)),
            -1
        )

    k = torch.LongTensor(test_data)
    X_test = input_features(k).cuda()

    net = Net(num_blocks=1, in_dim=X_test.shape[1]).cuda()
    net.load_state_dict(torch.load(args.model))
    net.eval()

    out = net(X_test)
    prediction = torch.max(out, 1)[1]
    for _ in prediction.data.cpu().numpy():
        print(_)
