import os
import argparse
from random import seed, randint
from math import log
import pickle
import datetime

import numpy as np
import networkx as nx

from utils import logger
from page_rank import page_rank
from negative_sampling.negative_sampling import NegativeSampler

seed(1)
VECTOR_LENGTH = 128


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1")
    parser.add_argument(
        '-n', '--embeddings', type=str,
        help="Node embeddings filename"
    )
    parser.add_argument(
        '-pr', '--page_rank', type=str,
        help="Page rank filename"
    )
    parser.add_argument(
        '-de', '--doc_embeddings', type=str,
        help="Doc embeddings filename"
    )
    parser.add_argument(
        '-nt', '--embedding_type', type=str,
        help="Node embeddings type"
    )
    parser.add_argument(
        '-train', '--train_edges', type=str,
        required=True,
        help="Training edges filename"
    )
    parser.add_argument(
        '-te', '--test_edges', type=str,
        required=True,
        help="Test edges filename"
    )
    parser.add_argument(
        '-fe', '--false_edges', type=str,
        help="False edges filename"
    )
    parser.add_argument(
        '-time', '--time', type=str,
        default=None,
        help="Time filename"
    )
    parser.add_argument(
        '-od', '--output_dir', type=str,
        help="Output directory name"
    )
    parser.add_argument(
        '-o', '--output_filename', type=str,
        help="Output filename"
    )
    parser.add_argument(
        '-d', '--feature_dimension', type=int,
        default=128,
        help="Dimension of embedding features"
    )
    args = parser.parse_args()
    return args


class Node():

    def __init__(
        self,
        idx: int
    ):
        """
        Args:
            idx: index
            vector: embedded vector for the node
        """
        self.idx = idx
        self.nodes_to = []
        self.nodes_from = []
        self.v = []
        self.time = None

    def add_edge(self, node_to_idx: int):
        self.nodes_to.append(node_to_idx)

    def add_node_from(self, node_from_idx: int):
        self.nodes_from.append(node_from_idx)

    def set_vector(self, v):
        self.v = v

    def concat_vector(self, v):
        self.v = self.v + v


class Graph():

    def __init__(
        self,
        train_edge_filename: str,
        test_edge_filename: str,
        nodes_embedding_filename=None,
        nodes_embedding_type="PCTADW",
        doc_embedding_filename=None,
        dim=128,
        directed=True,
        pg_vector_filename='page_rank.pickle',
        false_edges_filename='false_edges.txt',
        time_filename=None
    ):
        self.dim = dim
        self.directed = directed
        self.time_filename = time_filename
        self.nodes_embedding_type = nodes_embedding_type
        self.pg_vector_filename = pg_vector_filename
        self.false_edges_filename = false_edges_filename

        logger.info("Loading edges ..")
        self.train_edges = self.read_edges_from_file(train_edge_filename)
        self.edges = self.train_edges
        self.edge_number = len(self.edges)
        self.test_edges = self.read_edges_from_file(test_edge_filename)
        self.test_dummy_labels = [
            0  # if i < len(self.test_edges) / 2 else 1
            for i in range(len(self.test_edges))
        ]

        logger.info("Reading nodes ..")
        self.nodes_train = self._read_nodes(self.edges)
        self.nodes_test = self._read_nodes(self.test_edges)
        self.nodes_num = max([max(self.nodes_train), max(self.nodes_test)])
        self.nodes = {}
        for i in range(1, self.nodes_num+1):
            self.nodes[i] = Node(i)
        logger.info(f"Max node id {self.nodes_num}")

        logger.info("Connecting nodes and edges ..")
        self._build_edges()

        logger.info("Createing networkx graph ..")
        self.G = nx.DiGraph() if directed else nx.Graph()
        self._build_graph()

        if nodes_embedding_filename is not None:
            logger.info("Loading node embedding ..")
            self.set_node_embedding_from_file(nodes_embedding_filename)

        if doc_embedding_filename is not None:
            logger.info("Loading doc embedding ..")
            self.set_doc_embedding_from_file(doc_embedding_filename)
        logger.info("Generateing false edges ..")
        self.false_edges = self._generate_false_edges(self.edge_number)

        self.all_edges = self.edges + self.false_edges
        self.all_labels = [
            1 if x < self.edge_number else 0
            for x in range(len(self.all_edges))
        ]

        if time_filename is not None:
            self._load_time(time_filename)

    def _load_time(self, time_filename):
        with open(time_filename, 'r') as fin:
            lines = fin.readlines()
            for i, line in enumerate(lines):
                node_idx = i + 1
                s = line.strip()
                time = datetime.datetime.strptime(
                        s, "%Y-%m-%d %H:%M:%S"
                ).timestamp()
                self.nodes[node_idx].time = time

    def _build_graph(self):
        self.G.add_nodes_from(range(1, self.nodes_num+1))
        self.G.add_edges_from(
            self.edges
        )
        if self.directed:
            # Condensed_G is a DAG so that topological sort could be done
            condensed_G = nx.condensation(self.G)
            node_sequence = nx.topological_sort(condensed_G)
            node_sequence = list(node_sequence)[::-1]
            node_sequence_dict = {}
            for i, node_id in enumerate(node_sequence):
                node_sequence_dict[node_id] = i
            self.nodes_to_seq = {}
            for n in self.G.node():
                mapped_id = condensed_G.graph['mapping'][n]
                self.nodes_to_seq[n] = node_sequence_dict[mapped_id]

    def prepare_data(self, val_edge_num=4000):
        logger.info("Preparing train/valid/test data ..")
        self.val_edge_num = val_edge_num
        self.training_edges_true = \
            self.edges[: -self.val_edge_num // 2]
        self.training_edges_false = \
            self.false_edges[self.val_edge_num // 2:]
        self.training_edges = \
            self.training_edges_true + self.training_edges_false
        self.training_labels = [
            1 if x < len(self.edges) - self.val_edge_num
            else 0 for x in range(len(self.training_edges))
        ]
        self.valid_edges = (
            self.edges[-self.val_edge_num // 2:] +
            self.false_edges[:self.val_edge_num // 2]
        )
        self.valid_labels = [
            1 if x < self.val_edge_num // 2
            else 0 for x in range(len(self.valid_edges))
        ]
        return (
            self.training_edges, self.training_labels,
            self.valid_edges, self.valid_labels
        )

    def edges_to_vectors(self, edges):
        vectors = []
        for (u, v) in edges:
            u_vector = self.nodes[u].v
            v_vector = self.nodes[v].v
            vector = u_vector + v_vector
            if len(vector) != self.dim * 2:
                import pdb
                pdb.set_trace()
                print(vector)
            if self.time_filename is not None:
                u_time = self.nodes[u].time
                v_time = self.nodes[v].time
                vector += [u_time, v_time, u_time - v_time]
            vectors.append(vector)
        return np.array(vectors)

    def _read_nodes(self, edges, generate_mapping=False):
        nodes = {}
        for edge in edges:
            for idx in edge:
                if idx not in nodes:
                    nodes[idx] = Node(idx)

        return nodes

    def _build_edges(self):
        for edge in self.edges:
            node_from_idx, node_to_idx = edge
            self.nodes[node_from_idx].add_edge(node_to_idx)
            self.nodes[node_to_idx].add_node_from(node_from_idx)

    def read_edges_from_file(self, edge_filename):
        edges = []
        with open(edge_filename, 'r') as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                node_from_idx, node_to_idx = tuple(map(int, line.split()))
                edges.append((node_from_idx, node_to_idx))
        return edges

    def set_doc_embedding_from_file(self, filename):
        with open(filename, 'r') as fin:
            lines = fin.readlines()
            if 1:
                for node_idx, line in enumerate(lines):
                    node_idx = node_idx + 1
                    vector = list(map(float, line.split()))
                    self.nodes[node_idx].concat_vector(vector)

    def set_node_embedding_from_file(self, filename):
        with open(filename, 'r') as fin:
            lines = fin.readlines()
            # Deep walk format
            if len(lines[0].split()) == 2:
                nodes_num, vector_length = list(map(int, lines[0].split()))
                assert self.dim == vector_length, \
                    f"dim {self.dim} != {vector_length}"
                for line in lines[1:]:
                    line = list(map(float, line.split()))
                    node_idx = int(line[0])
                    if self.nodes_embedding_type == 'pte':
                        node_idx += 1
                    vector = line[1:]
                    assert len(vector) == int(vector_length), \
                        f"{len(vector)} != {vector_length}, vector {vector}"
                    self.nodes[node_idx].set_vector(vector)

            else:
                # Walklets format
                if lines[0][0] == 'x':
                    lines = lines[1:]
                    for line in lines:
                        node_idx_old = int(float(line.split(',')[0]))
                        vector = list(map(float, line.split(',')[1:]))
                        assert self.dim == len(vector), \
                            f"dim {self.dim} != {len(vector)}"
                        self.nodes[node_idx_old].set_vector(vector)

                else:
                    # PRUNE format
                    for node_idx, line in enumerate(lines):
                        vector = list(map(float, line.split(',')))
                        assert self.dim == len(vector), \
                            f"dim {self.dim} != {len(vector)}"
                        self.nodes[node_idx].set_vector(vector)

    def _generate_random_edge(self):
        u = randint(0, self.nodes_num - 1)
        v = randint(0, self.nodes_num - 1)
        return (u, v)

    def _get_vector(self, idx, nodes):
        if idx in nodes:
            return nodes[idx].v
        else:
            return [0] * VECTOR_LENGTH

    def _generate_false_edges(self, num, save=True):
        if os.path.exists(self.false_edges_filename):
            logger.info(
                f"Loading false edges from {self.false_edges_filename}")
            false_edges = self.read_edges_from_file(self.false_edges_filename)
        else:
            negativeSampler = NegativeSampler(
                self.get_features(), np.array(self.train_edges))
            false_edges = negativeSampler.sample()
            false_edges = [(u+1, v+1) for (u, v) in false_edges]
            with open(self.false_edges_filename, 'w') as fout:
                for edge in false_edges:
                    fout.write(f"{edge[0]} {edge[1]}\n")
        return false_edges

        """
        false_edges = []
        while len(false_edges) < num:
            if len(false_edges) % 1000 == 0:
                logger.debug(len(false_edges))
            u, v = self._generate_random_edge()
            count = 0
            while 1:
                if (u in self.nodes_train and
                        v in self.nodes_train and
                        v not in self.nodes[u].nodes_to and
                        self.nodes_to_seq[u] > self.nodes_to_seq[v]):
                    break
                u, v = self._generate_random_edge()
                count += 1
                assert count < 1000
            false_edges.append((u, v))
        if save:
            with open('false_edges.txt', 'w') as fout:
                for (u, v) in false_edges:
                    fout.write(f"{u} {v}\n")
                logger.info("false_edges.txt written")
        return false_edges
        """

    def get_adj_matrix(self):
        adj_matrix = np.zeros((self.nodes_num, self.nodes_num))
        for node_idx, node in self.nodes.items():
            for node_to_idx in node.nodes_to:
                adj_matrix[node_idx-1][node_to_idx-1] = 1
        for (u, v) in self.test_edges:
            if u in self.nodes and v in self.nodes:
                adj_matrix[u-1][v-1] = 0.5

        return adj_matrix

    def get_features(self):
        features = []
        for i, node in self.nodes.items():
            features.append(node.v)
        features = np.array(features)
        return features

    def find_x2k2y_nodes(self, node_x_idx, node_y_idx):
        nodes = []
        node_x = self.nodes[node_x_idx]
        for i, node_k_idx in enumerate(node_x.nodes_to):
            node_k = self.nodes[node_k_idx]
            if node_y_idx in node_k.nodes_to:
                nodes.append(node_k)
            elif node_y_idx in node_k.nodes_from:
                nodes.append(node_k)
        return nodes

    def find_common_neighbors(self, node_x_idx, node_y_idx):
        if node_x_idx == node_y_idx:
            return []
        node_x = self.nodes[node_x_idx]
        node_y = self.nodes[node_y_idx]
        x_neighbors = node_x.nodes_to + node_x.nodes_from
        y_neighbors = node_y.nodes_to + node_y.nodes_from
        common_neighbors_idx = list(set(x_neighbors).intersection(y_neighbors))
        common_neighbors = [
            self.nodes[idx] for idx in common_neighbors_idx
        ]
        return common_neighbors

    def _apply_prediction(self, func, ebunch=None):
        """Applies the given function to each edge in the specified iterable
        of edges.

        `G` is an instance of :class:`networkx.Graph`.

        `func` is a function on two inputs, each of which is a node in the
        graph. The function can return anything, but it should return a
        value representing a prediction of the likelihood of a "link"
        joining the two nodes.

        `ebunch` is an iterable of pairs of nodes. If not specified, all
        non-edges in the graph `G` will be used.

        """
        if ebunch is None:
            ebunch = self.training_edges
        return ((u, v, func(u, v)) for u, v in ebunch)

    def adamic_adar_index(self, ebunch=None, asymmetric=False):

        def predict(u, v):
            if asymmetric:
                return sum(
                    1 / log(len(w.nodes_from) + len(w.nodes_to))
                    for w in self.find_x2k2y_nodes(u, v)
                )
            else:
                try:
                    return sum(
                        1 / log(len(w.nodes_from) + len(w.nodes_to))
                        for w in self.find_common_neighbors(u, v)
                    )
                except:
                    import pdb
                    pdb.set_trace()
                    print("")
        return self._apply_prediction(predict, ebunch)

    def compute_score_matrix(self, method, symmetric=True):
        matrix = np.zeros((self.nodes_num, self.nodes_num))
        for u, v, p in method():
            matrix[u][v] = p
            if symmetric:
                matrix[v][u] = p

        matrix = matrix / matrix.max()
        return matrix

    def save_edges(self, edges, output_filename):
        with open(output_filename, 'w') as fout:
            for (u, v) in edges:
                fout.write(f"{u}\tcite\t{v}\n")

    def predict_links(self, method, test_edges, test_labels, threshold):
        result_generator = method(self.G, test_edges)

        results = []
        correct = 0
        for i, (u, v, p) in enumerate(result_generator):
            results.append(p)
            result = 1 if p > threshold else 0
            if result == test_labels[i]:
                correct += 1
        acc = correct / len(test_edges)
        logger.info(
            f"Accuracy: {correct/len(test_edges)*100:.4}% "
            f"({correct}/{len(test_edges)})"
        )
        results = np.array(results)
        return results / results.max(), acc

    def find_middle_threshold(self, results):
        sorted_results = sorted(results)
        th = sorted_results[len(sorted_results)//2]
        logger.info(f"Middle threshold = {th}")
        return th

    def find_best_threshold(self, results, test_edges, test_labels):
        best_acc = 0
        best_th = 0
        for th in range(-1000, 1000, 5):
            threshold = th / 1000
            correct = 0
            for i, p in enumerate(results):
                result = 1 if p > threshold else 0
                if result == test_labels[i]:
                    correct += 1
            acc = correct/len(test_edges)
            logger.debug(
                f"th = {th}: {acc*100:.4}% "
                f"({correct}/{len(test_edges)})"
            )
            if acc > best_acc:
                best_acc = acc
                best_th = threshold
        logger.info(f"When th = {best_th}, acc = {best_acc}")
        return best_th, best_acc

    def write_result(self, output_filename, results, th):
        with open(output_filename, 'w') as fout:
            for p in results:
                result = 1 if p > th else 0
                fout.write(f"{result}\n")
        logger.info(f"Result {output_filename} written.")

    def cos_sim(self, G, edges):
        results = []
        for (u, v) in edges:
            if u in self.nodes and v in self.nodes:
                u_vector = self.nodes[u].v
                v_vector = self.nodes[v].v
                dot_value = (
                    np.dot(u_vector, v_vector) /
                    np.linalg.norm(u_vector) /
                    np.linalg.norm(v_vector)
                )
                assert dot_value is not None
            else:
                raise IOError()
            result = (u, v, dot_value)
            results.append(result)
        return results

    def cos_sim_with_pagerank(self, G, edges):
        adj_matrix = self.get_adj_matrix()
        if os.path.exists(self.pg_vector_filename):
            logger.info(f"Loading pagerank from {self.pg_vector_filename}")
            with open(self.pg_vector_filename, 'rb') as fin:
                pg_vector = pickle.load(fin)
        else:
            pg_vector = page_rank(adj_matrix)
            with open(self.pg_vector_filename, 'wb') as fout:
                pickle.dump(pg_vector, fout)
        cos_sim_results = self.cos_sim(G, edges)
        results = list(map(lambda x: (x[0], x[1], x[2]*pg_vector[x[1]-1]),
                           cos_sim_results))
        return results


if __name__ == "__main__":
    args = parse_args()
    graph = Graph(
        args.train_edges,
        args.test_edges, args.embeddings,
        dim=args.feature_dimension,
        nodes_embedding_type=args.embedding_type,
        doc_embedding_filename=args.doc_embeddings,
        directed=True,
        false_edges_filename=args.false_edges
    )
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    graph.prepare_data(val_edge_num=10000)

    methods = [
        graph.cos_sim,
        graph.cos_sim_with_pagerank,
        nx.adamic_adar_index,
        nx.jaccard_coefficient,
        nx.resource_allocation_index,
        nx.preferential_attachment
    ]
    for method in methods:
        if method != graph.cos_sim and method != graph.cos_sim_with_pagerank:
            graph.G = graph.G.to_undirected()
        try:
            logger.info(f"--------- Method: {method.__name__}")
        except Exception:
            logger.warning("Method name unknown")

        # Find best threshold for validation set
        val_edges = graph.valid_edges
        val_labels = graph.valid_labels
        val_results, val_acc = graph.predict_links(
            method, val_edges, val_labels, 0)
        best_th, best_acc = \
            graph.find_best_threshold(val_results, val_edges, val_labels)

        # Apply on test set
        test_edges = graph.test_edges
        test_labels = graph.test_dummy_labels
        for edge in test_edges:
            for n in edge:
                try:
                    deg = graph.G.degree(n)
                except Exception as err:
                    # It is said unseen nodes are 1
                    graph.G.add_edge(*edge)

        results, best_th_acc = graph.predict_links(
            method, test_edges, test_labels, best_th)
        middle_th = graph.find_middle_threshold(results)
        results, val_acc = graph.predict_links(
            method, val_edges, val_labels, middle_th
        )
        logger.info("Test data zero percentage:")
        results, acc = graph.predict_links(
            method, test_edges, test_labels, middle_th
        )
        output_filename = (
            f"{method.__name__}_dim{args.feature_dimension}_"
            f"th{middle_th}_val_acc{val_acc}.txt"
        )
        output_filename = os.path.join(args.output_dir, output_filename)
        graph.write_result(output_filename, results, middle_th)
