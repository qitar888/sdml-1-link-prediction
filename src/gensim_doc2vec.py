import os
import argparse

import numpy as np
from numpy.linalg import norm
# from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument

from utils import (
    logger, save_labels, find_middle_threshold,
    make_dirs
)


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1-2")
    parser.add_argument(
        '-n', '--embeddings', type=str,
        help="Node embeddings filename"
    )
    parser.add_argument(
        '-train', '--train_edges', type=str,
        # required=True,
        help="Training edges filename"
    )
    parser.add_argument(
        '-te', '--test_edges', type=str,
        required=True,
        help="Test edges filename"
    )
    parser.add_argument(
        '-od', '--output_dir', type=str,
        required=True,
        help="Output directory name"
    )
    parser.add_argument(
        '-dif', '--doc_inline_filename', type=str,
        help="Node title and abstract in lines file "
    )
    parser.add_argument(
        '-lf', '--load_filename', type=str,
        help="Load fileanme"
    )
    parser.add_argument(
        '-dd', '--dump_dir', type=str,
        help="Directory to dump"
    )
    parser.add_argument(
        '-dim', '--dimensions', type=int,
        required=True,
        help="Vector dimensions"
    )
    args = parser.parse_args()
    return args


def main(args):
    if args.load_filename is not None:
        assert os.path.exists(args.load_filename)
        logger.info(f"Loading docs from {args.load_filename} ..")
        vectors = np.loadtxt(args.load_filename)

    else:
        logger.info("Loading docs and calculating doc representation ..")
        with open(args.doc_inline_filename, 'r') as fin:
            doc_lines = fin.readlines()
            doc_lines = [doc_line.split() for doc_line in doc_lines]

        documents = [
            TaggedDocument(doc, [i]) for i, doc in enumerate(doc_lines)
        ]
        model = Doc2Vec(
            documents,
            vector_size=args.dimensions,
            epochs=5,
            workers=10
        )

        vectors = []
        for doc_line in doc_lines:
            vector = model.infer_vector(doc_line)
            vectors.append(vector)

        dump_filename = os.path.join(
            args.dump_dir,
            f"gensim_doc2vec_dim{args.dimensions}.embeddings"
        )
        logger.info(f"Saveing vectors to {dump_filename} ..")
        np.savetxt(dump_filename, vectors)

    logger.info("Reading training edges ..")
    with open(args.train_edges, 'r') as fin:
        train_edges = fin.readlines()
    logger.info("Reading testing edges ..")
    with open(args.test_edges, 'r') as fin:
        test_edges = fin.readlines()

    logger.info("Calculating similarities")
    results = []
    for line in test_edges:
        u, v = list(map(int, line.split()))
        u_v = vectors[u-1]
        v_v = vectors[v-1]
        similarity = np.dot(u_v, v_v) / norm(u_v) / norm(v_v)
        results.append(similarity)
    middle_th = find_middle_threshold(results)

    count = 0
    for line in train_edges:
        u, v = list(map(int, line.split()))
        u_v = vectors[u-1]
        v_v = vectors[v-1]
        similarity = np.dot(u_v, v_v) / norm(u_v) / norm(v_v)
        if similarity > middle_th:
            count += 1
    logger.info(f"Train acc on middle th: {count/len(train_edges)}")
    output_filename = os.path.join(
        args.output_dir,
        f"gensim_doc2vec_dim{args.dimensions}_th{middle_th:.04}.txt"
    )
    save_labels(results, output_filename, middle_th)


if __name__ == "__main__":
    args = parse_args()
    make_dirs(args.dump_dir)
    make_dirs(args.output_dir)
    main(args)
