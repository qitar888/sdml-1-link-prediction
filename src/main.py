import os
import argparse

from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
# import numpy as np

from graph2 import Graph
from utils import logger, save_labels


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1")
    parser.add_argument(
        '-time', '--time', type=str,
        default=None,
        help="Time filename"
    )
    parser.add_argument(
        '-fe', '--false_edges', type=str,
        help="False edges filename"
    )
    parser.add_argument(
        '-n', '--embeddings', type=str,
        help="Node embeddings filename"
    )
    parser.add_argument(
        '-de', '--doc_embeddings', type=str,
        help="Doc embeddings filename"
    )
    parser.add_argument(
        '-nt', '--embedding_type', type=str,
        help="Node embeddings type"
    )
    parser.add_argument(
        '-train', '--train_edges', type=str,
        required=True,
        help="Training edges filename"
    )
    parser.add_argument(
        '-te', '--test_edges', type=str,
        required=True,
        help="Test edges filename"
    )
    parser.add_argument(
        '-od', '--output_dir', type=str,
        help="Output directory name"
    )
    parser.add_argument(
        '-o', '--output_filename', type=str,
        help="Output filename"
    )
    parser.add_argument(
        '-d', '--feature_dimension', type=int,
        default=128,
        help="Dimension of embedding features"
    )
    args = parser.parse_args()
    return args


def classify(clf, xs, ys, val_xs, val_ys, test_xs, test_ys):
    logger.info(f"Doing {clf.__class__.__name__} ..")
    clf.fit(xs, ys)
    val_acc = clf.score(val_xs, val_ys)
    logger.info(f"Val acc = {val_acc}")
    # results = clf.predict(test_xs)
    results = clf.predict_proba(test_xs)
    return results, val_acc


if __name__ == "__main__":
    args = parse_args()
    graph = Graph(
        args.train_edges,
        args.test_edges,
        args.embeddings,
        dim=args.feature_dimension,
        doc_embedding_filename=args.doc_embeddings,
        nodes_embedding_type=args.embedding_type,
        directed=True,
        time_filename=args.time,
        false_edges_filename=args.false_edges
    )
    logger.info(f"Node number: {len(graph.nodes)}")
    classifiers = [
        RandomForestClassifier(
            n_estimators=200, max_depth=10, random_state=42,
            n_jobs=-1,
        ),
        LogisticRegression(
            solver='lbfgs', random_state=42, n_jobs=-1),
        DecisionTreeClassifier(max_depth=5),
        SVC(kernel="linear", C=0.025, max_iter=10000,
            probability=True),
        KNeighborsClassifier(n_neighbors=5, n_jobs=20),
        # SVC(gamma='auto', verbose=True, max_iter=500)
    ]

    graph.prepare_data(10000)
    ys = graph.training_labels
    xs = graph.edges_to_vectors(graph.training_edges)

    val_ys = graph.valid_labels
    val_xs = graph.edges_to_vectors(graph.valid_edges)

    test_ys = graph.test_dummy_labels
    test_xs = graph.edges_to_vectors(graph.test_edges)

    logger.info(f"len(ys) = {len(ys)}")
    logger.info(f"len(val_ys) = {len(val_ys)}")
    logger.info(f"len(test_ys) = {len(test_ys)}")
    for clf in classifiers:
        results, val_acc = classify(
            clf, xs, ys, val_xs, val_ys, test_xs, test_ys)
        if args.doc_embeddings is not None:
            embedding_name = args.doc_embeddings.split('/')[-1]
        else:
            embedding_name = args.embeddings.split('/')[-1]
        output_filename = os.path.join(
            args.output_dir,
            (f"{embedding_name}_"
             f"{clf.__class__.__name__}_val_acc_{val_acc}_.txt")
        )
        middle_th = graph.find_middle_threshold(results[:, 1])
        save_labels(results[:, 1], output_filename, th=middle_th)
