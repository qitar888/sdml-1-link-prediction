import os
import argparse

from utils import get_filename_int, logger


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1-2")
    parser.add_argument(
        '-xd', '--xml_dir_name', type=str,
        help="XML directory name"
    )
    parser.add_argument(
        '-o', '--output_filename', type=str,
        help="Output filename"
    )
    parser.add_argument(
        '-to', '--title_only',
        action='store_true',
        help="Title only"
    )
    args = parser.parse_args()
    return args


def doc_to_lines(dir_name, output_filename, title_only=False):
    with open(output_filename, 'w') as fout:
        for root, dirs, filenames in os.walk(dir_name):
            for filename in sorted(filenames, key=get_filename_int):
                filename = os.path.join(
                    root, filename
                )
                with open(filename, 'r') as fin:
                    # idx = filename.split('.')[0]
                    xml = fin.read()
                    words = []
                    for word in xml.split():
                        if "<abstract>"in word and title_only:
                            break
                        if "<" not in word:
                            words.append(f"{word} ")
                    line = "".join(words) + "\n"
                    fout.write(line)
                    logger.info(f"{filename} {line}")
    logger.info(f"{output_filename} written")


if __name__ == "__main__":
    args = parse_args()
    doc_to_lines(
        args.xml_dir_name,
        args.output_filename,
        args.title_only
    )
