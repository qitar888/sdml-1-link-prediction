import numpy as np


class NegativeSampler(object):

    def __init__(self, embeddings, edges, time_sequences=None):
        """
        Args:
            embeddings(np.2darray(num_nodes, emb_dim)): the embeddings for all
                the nodes.
            edges(np.2darray(training_data, 2)): each row represents a positive
                links from row[0] to row[1].
            time_sequences([int]): a list of node_ids indicating the time
                topology of nodes. If not provided, a topological sort would be
                done instead.
        ===
        Examples:
            embs = np.array([[1., 0.],
                             [0., 1.]])
            edges = np.array([[1, 0]])
            t = [0, 1]

            neg = NegativeSampler(embs, edges)
            # or
            neg = NegativeSampler(embs, edges, time_sequences=t)
        """
        self.embeddings = embeddings
        self.num_nodes = self.embeddings.shape[0]

        self.edges = edges - np.ones(edges.shape, dtype=int) # from 1-based to 0-based

        self.in_degree = np.zeros(embeddings.shape[0])
        for i, _ in enumerate(self.in_degree):
            self.in_degree[i] = self.edges[self.edges[:, 1] == i].shape[0]

        self.adjacency_lists = [[]] * embeddings.shape[0]
        for idx, _ in enumerate(self.adjacency_lists):
            self.adjacency_lists[idx] = \
                self.edges[self.edges[:, 0] == idx][:, 1]
            self.adjacency_lists[idx] = np.unique(self.adjacency_lists[idx])

        if time_sequences is not None:
            self.t = time_sequences
        else:
            self.t = self.topological_sort()

    def topological_sort(self):
        """
        Returns:
          [int]: a list of topological sorted node_ids.
        """
        topological_list = []
        seen_node = set()

        def dfs(i):
            seen_node.add(i)
            for j in self.adjacency_lists[i]:
                if j not in seen_node:
                    dfs(j)
            topological_list.append(i)

        for i in range(self.num_nodes):
            if i not in seen_node and self.in_degree[i] == 0:
                dfs(i)

        return topological_list

    def nearest_neighbors(self, node_id, k, from_nodes=None):
        """
        Args:
            node_id(int): the node id to extract non-positive nearest
            neighbor (in vector space).
            k(int): the number of neighbors to extract.
            from_nodes([int]): the nodes to choose from in the return.
        Returns:
            [int]: the ids of the nearest neighbors, len() = k.
        """
        if from_nodes == []:
            return np.array([])

        mask = np.zeros(self.num_nodes, dtype=bool)
        mask[from_nodes] = True
        candidate_nodes = np.arange(self.num_nodes)[mask]

        cossim_value = np.vectorize(
            lambda i: self.cosine_similarity(
                self.embeddings[i], self.embeddings[node_id]
            )
        )(candidate_nodes)
        sorted_index = np.argsort(cossim_value)[::-1]
        candidate_nodes = np.array(candidate_nodes)[sorted_index]
        return candidate_nodes[:k]

    def cosine_similarity(self, a, b):
        """
        Args:
            a(np.array): vector 1.
            b(np.array): vector 2.
        Returns:
            float: CosSim(a, b)
        """
        return np.dot(a, b) / (np.sqrt(np.dot(a, a)) * np.sqrt(np.dot(b, b)))

    def sample(self):
        """
        Returns:
            np.2darray(len(negative_data), 2): each row is a negative edge
            given existing pos edges.
        """
        output = []
        for i, elem in enumerate(self.t):
            candidates = \
                list(set(self.t[:i]) - set(self.adjacency_lists[elem]))
            z = self.nearest_neighbors(
                elem, self.adjacency_lists[elem].shape[0],
                from_nodes=candidates
            )
            output.append([[elem, j] for j in z])
        output = np.array([val for row in output for val in row])
        return output + np.ones(output.shape, dtype=int) # from 0-based to 1-based
