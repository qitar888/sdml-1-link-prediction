import os
import logging

import numpy as np


LOGGING_LEVEL = logging.INFO
logging.basicConfig(
        level=LOGGING_LEVEL,
        format=('[%(asctime)s] {%(filename)s:%(lineno)d} '
                '%(levelname)s - %(message)s'),
)
logger = logging.getLogger(__name__)


def get_filename_int(filename):
    return int(filename.split('.')[0])


def save_labels(p_label, output_filename, th):
    with open(output_filename, 'w') as f_out:
        for label in p_label:
            if float(label) >= th:
                output = 1
            else:
                output = 0
            f_out.write(f"{output}\n")
    logger.info(f"Output labels saved to {output_filename}")


def find_middle_threshold(results):
    sorted_results = sorted(results)
    th = sorted_results[len(sorted_results)//2]
    logger.info(f"Middle threshold = {th}")
    return th


def make_dirs(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    logger.info(f"Made directory {dir_name}")


def load_embeddings(filename, embeddings_type="doc2vec"):
    if embeddings_type == 'doc2vec':
        embeddings = np.loadtxt(filename)
    else:
        embeddings = []
        with open(filename, 'r') as fin:
            lines = fin.readlines()
            nodes_num, vector_length = list(map(int, lines[0].split()))
            for line in lines[1:]:
                line = list(map(float, line.split()))
                vector = line[1:]
                embeddings.append(vector)
        embeddings = np.array(embeddings)
    return embeddings
