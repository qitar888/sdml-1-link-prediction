from __future__ import division
from __future__ import print_function

import time
import argparse
import os
import sys
parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # NOQA
sys.path.insert(0, parentdir)  # NOQA

import numpy as np
# import scipy.io as sio

import torch
import torch.nn.functional as F
import torch.optim as optim

from lib.pygcn.pygcn.utils import accuracy
from lib.pygcn.pygcn.models import GCN

from graph_dataset2 import GraphDataset
from utils import make_dirs

# Training settings
parser = argparse.ArgumentParser()
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='Disables CUDA training.')
parser.add_argument('--fastmode', action='store_true', default=False,
                    help='Validate during training pass.')
parser.add_argument('--seed', type=int, default=42, help='Random seed.')
parser.add_argument('--epochs', type=int, default=200,
                    help='Number of epochs to train.')
parser.add_argument('--lr', type=float, default=0.01,
                    help='Initial learning rate.')
parser.add_argument('--weight_decay', type=float, default=5e-4,
                    help='Weight decay (L2 loss on parameters).')
parser.add_argument('--hidden', type=int, default=16,
                    help='Number of hidden units.')
parser.add_argument('--dropout', type=float, default=0.5,
                    help='Dropout rate (1 - keep probability).')
parser.add_argument(
    '-de', '--doc_embeddings', type=str,
    help="Doc embeddings filename"
)
parser.add_argument(
    '-nt', '--embedding_type', type=str,
    help="Node embeddings type"
)
parser.add_argument(
    '-train', '--train_edges', type=str,
    required=True,
    help="Training edges filename"
)
parser.add_argument(
    '-te', '--test_edges', type=str,
    required=True,
    help="Test edges filename"
)
parser.add_argument(
    '-dim', '--feature_dimension', type=int,
    required=True,
    help="Embedding feature dimension"
)
parser.add_argument(
    '-od', '--output_dir', type=str,
    required=True,
    help="Output directory"
)

args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

np.random.seed(args.seed)
torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


graph = GraphDataset(
    args.train_edges,
    args.test_edges, None,
    dim=args.feature_dimension,
    nodes_embedding_type=args.embedding_type,
    doc_embedding_filename=args.doc_embeddings,
    directed=True
)

# Load data
adj, features, labels, idx_train, idx_val, idx_test = \
    graph.load_data_in_pygcn_format()
# sio.savemat("adj.mat", {"graph_sparse": adj})


# Model and optimizer
model = GCN(nfeat=features.shape[1],
            nhid=args.hidden,
            nclass=labels.max().item() + 1,
            dropout=args.dropout)
optimizer = optim.Adam(model.parameters(),
                       lr=args.lr, weight_decay=args.weight_decay)

if args.cuda:
    model.cuda()
    features = features.cuda()
    adj = adj.cuda()
    labels = labels.cuda()
    idx_train = idx_train.cuda()
    idx_val = idx_val.cuda()
    idx_test = idx_test.cuda()


def train(epoch):
    t = time.time()
    model.train()
    optimizer.zero_grad()
    output = model(features, adj)
    loss_train = F.nll_loss(output[idx_train], labels[idx_train])
    acc_train = accuracy(output[idx_train], labels[idx_train])
    loss_train.backward()
    optimizer.step()

    if not args.fastmode:
        # Evaluate validation set performance separately,
        # deactivates dropout during validation run.
        model.eval()
        output = model(features, adj)

    loss_val = F.nll_loss(output[idx_val], labels[idx_val])
    acc_val = accuracy(output[idx_val], labels[idx_val])
    print('Epoch: {:04d}'.format(epoch+1),
          'loss_train: {:.4f}'.format(loss_train.item()),
          'acc_train: {:.4f}'.format(acc_train.item()),
          'loss_val: {:.4f}'.format(loss_val.item()),
          'acc_val: {:.4f}'.format(acc_val.item()),
          'time: {:.4f}s'.format(time.time() - t))


def test():
    model.eval()
    hidden = model.hidden(features, adj)
    embeddings = hidden.cpu().detach().numpy()
    make_dirs(args.output_dir)
    output_filename = os.path.join(
        args.output_dir,
        f"pygcn_dim{args.hidden}_{args.doc_embeddings.split('/')[-1]}"
        f".embeddings"
    )
    with open(output_filename, 'w') as fout:
        fout.write(f"{embeddings.shape[0]} {embeddings.shape[1]}\n")
        for i in range(graph.nodes_num):
            fout.write(f"{i+1} ")
            for e in embeddings[i]:
                fout.write(f"{e} ")
            fout.write("\n")
        print(f"{output_filename} written")

    output = model(features, adj)
    loss_test = F.nll_loss(output[idx_test], labels[idx_test])
    acc_test = accuracy(output[idx_test], labels[idx_test])
    print("Test set results:",
          "loss= {:.4f}".format(loss_test.item()),
          "accuracy= {:.4f}".format(acc_test.item()))


# Train model
t_total = time.time()
for epoch in range(args.epochs):
    train(epoch)
print("Optimization Finished!")
print("Total time elapsed: {:.4f}s".format(time.time() - t_total))

# Testing
test()
