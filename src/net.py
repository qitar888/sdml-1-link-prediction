import torch
import torch.nn as nn

class Block(nn.Module):
    def __init__(self, unit):
        super(Block, self).__init__()

        self.relu = nn.ReLU()

        self.net = nn.Sequential(
            nn.BatchNorm1d(unit),
            nn.ReLU(),
            nn.Linear(unit, unit),
            nn.BatchNorm1d(unit),
            nn.ReLU(),
            nn.Linear(unit, unit))

    def forward(self, x):
        x = self.net(x) + x
        x = self.relu(x)
        return x


class Net(nn.Module):
    """ Net, borrows the idea from ResNet.
    args:
        in_dim(int): the input dimension
        num_blocks(int): the basic blocks, for every block the residual is directly pass thru.
    """
    def __init__(self, in_dim=128, num_blocks=0):
        super(Net, self).__init__()

        half = int(in_dim/2)
        oneeighth = int(in_dim/8)

        self.net = nn.Sequential(
            nn.Linear(in_dim, half),
            nn.ReLU(),
            nn.BatchNorm1d(half),

            *[Block(half)]*num_blocks,

            nn.Dropout(p=0.3),

            nn.Linear(half, oneeighth),
            nn.ReLU(),
            nn.BatchNorm1d(oneeighth),

            *[Block(oneeighth)]*num_blocks,

            nn.Dropout(p=0.6),

            nn.Linear(oneeighth, 2),
            nn.PReLU())

    def forward(self, x):
        x = self.net(x)
        return x
