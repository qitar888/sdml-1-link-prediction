import argparse
import datetime
import os
from datetime import datetime
import dateutil.parser

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_path", type=str)
    parser.add_argument("-o", "--output_path", type=str)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    output = []

    with open(args.input_path, "r") as infile:
        for line in infile:
            output.append(dateutil.parser.parse(line))

    output = list(map(int,map(datetime.timestamp, output)))

    with open(args.output_path, "w") as outfile:
        for line in output:
            print(line, file=outfile)
