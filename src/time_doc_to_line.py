import os
import argparse
from datetime import datetime
import dateutil.parser

from utils import logger, get_filename_int


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1-2")
    parser.add_argument(
        '-xd', '--xml_dir_name', type=str,
        required=True,
        help="XML directory name"
    )
    parser.add_argument(
        '-dof', '--doc_output_fn', type=str,
        required=True,
        help="Documents output filename"
    )
    parser.add_argument(
        '-tof', '--time_output_fn', type=str,
        required=True,
        help="Time output filename"
    )
    args = parser.parse_args()
    return args


def doc_to_lines(dir_name, doc_output_fn, time_output_fn):
    with open(doc_output_fn, 'w') as fout_doc, \
            open(time_output_fn, 'w') as fout_time:
        for root, dirs, filenames in os.walk(dir_name):
            for filename in sorted(filenames, key=get_filename_int):
                filename = os.path.join(
                    root, filename
                )
                with open(filename, 'r') as fin:
                    # idx = filename.split('.')[0]
                    xml = fin.read()
                    time_str, doc = xml.split('\n</date>\n\n<title>\n')
                    time_str_clean_list = time_str.split('\n')[1].split()
                    time_str_short = "".join(
                        [f"{x} " for x in time_str_clean_list[:-1]])
                    try:
                        time = dateutil.parser.parse(
                            time_str_short)
                    except:
                        time_str_short = "".join(
                            [f"{x} " for x in time_str_clean_list[:-2]])
                        time = dateutil.parser.parse(
                            time_str_short)

                    line = "".join(
                        [f"{x} " for x in doc.split()
                         if "<" not in x]
                    ) + "\n"
                    fout_doc.write(line)
                    fout_time.write(time.strftime("%Y-%m-%d %H:%M:%S") + "\n")
                    logger.info(
                            f'{filename} {time.strftime("%Y-%m-%d %H:%M:%S")} '
                            f'{line[:50]}')
    logger.info(f"{doc_output_fn} and {time_output_fn} written")


if __name__ == "__main__":
    args = parse_args()
    doc_to_lines(args.xml_dir_name, args.doc_output_fn, args.time_output_fn)
