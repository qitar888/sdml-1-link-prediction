import argparse

from graph2 import Graph
from utils import logger


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1-2")
    parser.add_argument(
        '-n', '--embeddings', type=str,
        help="Node embeddings filename"
    )
    parser.add_argument(
        '-ptrain', '--pte_train', type=str,
    )
    parser.add_argument(
        '-ptl', '--pte_train_label', type=str,
    )
    parser.add_argument(
        '-de', '--doc_embeddings', type=str,
        help="Doc embeddings filename"
    )
    parser.add_argument(
        '-nt', '--embedding_type', type=str,
        help="Node embeddings type"
    )
    parser.add_argument(
        '-train', '--train_edges', type=str,
        required=True,
        help="Training edges filename"
    )
    parser.add_argument(
        '-te', '--test_edges', type=str,
        required=True,
        help="Test edges filename"
    )
    parser.add_argument(
        '-od', '--output_dir', type=str,
        help="Output directory name"
    )
    parser.add_argument(
        '-o', '--output_filename', type=str,
        help="Output filename"
    )
    parser.add_argument(
        '-d', '--feature_dimension', type=int,
        default=128,
        help="Dimension of embedding features"
    )
    parser.add_argument(
        '-df', '--doc_file', type=str,
    )
    args = parser.parse_args()
    return args


def data_to_pte(args):
    with open(args.doc_file, 'r') as fin:
        docs = fin.readlines()

    graph = Graph(
        args.train_edges,
        args.test_edges, args.embeddings,
        dim=args.feature_dimension,
        nodes_embedding_type=args.embedding_type,
        doc_embedding_filename=args.doc_embeddings,
        directed=True
    )
    train_nodes = graph.nodes_train.keys()
    test_nodes = graph.nodes_test.keys()
    with open(args.pte_train, 'w') as fout_train, \
            open(args.pte_train_label, 'w') as fout_label:
        for n in train_nodes:
            logger.info(
                f"Train node {n} docs {docs[n-1][:5]} deg {graph.G.degree(n)}")
            fout_train.write(docs[n-1])
            fout_label.write(f"{graph.G.degree(n)}\n")


if __name__ == "__main__":
    args = parse_args()
    data_to_pte(args)
