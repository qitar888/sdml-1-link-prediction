import os

import numpy as np
import torch
import torch.nn as nn

from negative_sampling.negative_sampling import NegativeSampler
from utils import load_embeddings


class DataUtil(object):

    def __init__(
        self, train_path, timestamp_path, emb_path, emb_type="doc2vec"
    ):
        np.random.seed(7122)

        self.timestamp = np.loadtxt(timestamp_path, dtype=int)
        self.embs = load_embeddings(emb_path, emb_type)

        self.train, self.valid = self._split_valid(self._get_train(train_path))

    def get_embeddings(self):
        return nn.Embedding.from_pretrained(torch.FloatTensor(self.embs))

    # private
    def _get_train(self, train_path):
        pos = np.loadtxt(train_path, dtype=int)
        if os.path.isfile("data/t2-neg.txt"):
            neg = np.loadtxt("data/t2-neg.txt")
        else:
            negative_sampler = NegativeSampler(
                self.embs, pos, np.argsort(self.timestamp))
            neg = negative_sampler.sample()
            np.savetxt("data/t2-neg.txt", neg, fmt="%s")

        pos = pos - torch.ones(pos.shape).long()  # to 0-based
        neg = neg - torch.ones(neg.shape).long()  # to 0-based
        assert (pos < 0).sum() == 0
        assert (neg < 0).sum() == 0
        train = np.concatenate((
            np.c_[pos, np.ones(pos.shape[0])],            # row: [from, to, 1]
            np.c_[neg, np.zeros(neg.shape[0])]), axis=0)  # row: [from, to, 0]
        return train

    def _split_valid(self, array_like):
        split_type = 'random'  # opts = ['random', 'after_t']
        if split_type == 'random':
            alpha = .05  # valid percentage
            n = array_like.shape[0]
            mask = np.zeros(n, dtype=bool)
            mask[np.random.choice(n, int(n * alpha), replace=False)] = True
            return array_like[~mask], array_like[mask]
        # extract all nodes after time t as valid dataset
        elif split_type == 'after_t':
            pass
        else:
            raise
