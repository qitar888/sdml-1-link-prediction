import sys
import os
import argparse
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))  # NOQA

import torch
import torch.nn as nn
import torch.utils.data as Data
import torch.optim as optim
import torch.multiprocessing as mp
import time

from net import Net
import t2_neural_network.datautil as datautil


BATCH_SIZE = 32
MODEL_NAME = "net_params_task2_{}.pkl".format(time.strftime("%Y%m%d-%H%M%S"))


def parse_args():
    parser = argparse.ArgumentParser("STML 2018 HW1")
    parser.add_argument(
        '-train', '--train_edges', type=str,
        required=True,
        help="Training edges filename"
    )
    parser.add_argument(
        '-time', '--time', type=str,
        required=True,
        help="Posix time filename"
    )
    parser.add_argument(
        '-n', '--embeddings', type=str,
        required=True,
        help="Node embeddings filename"
    )
    parser.add_argument(
        '-nt', '--embeddings_type', type=str,
        required=True,
        help="Node embeddings type"
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    mp.set_start_method('spawn')
    torch.manual_seed(5269)

    d = datautil.DataUtil(
        train_path=args.train_edges,
        timestamp_path=args.time,
        emb_path=args.embeddings,
        emb_type=args.embeddings
    )

    embeddings = d.get_embeddings()

    def input_features(data):
        return torch.cat((
            embeddings(data[:, :2]).view(data.shape[0], -1),
            torch.FloatTensor(d.timestamp[data[:, :2]]),
            torch.unsqueeze(
                torch.FloatTensor(
                    d.timestamp[data[:, 0]] - d.timestamp[data[:, 1]]
                ), -1)),
            -1
        )

    k = torch.LongTensor(d.train)
    X_train = input_features(k).cuda()
    y_train = k[:, 2].cuda()

    k = torch.LongTensor(d.valid)
    X_valid = input_features(k).cuda()
    y_valid = k[:, 2].cuda()

    torch_dataset = Data.TensorDataset(X_train, y_train)
    loader = Data.DataLoader(
        dataset=torch_dataset,
        batch_size=BATCH_SIZE,
        shuffle=True,
        num_workers=0
    )

    net = Net(num_blocks=1, in_dim=X_train.shape[1]).cuda()
    loss_func = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=3e-4, weight_decay=1e-5)

    best_acc = 0
    for epoch in range(300):
        count = 0
        for step, (batch_X, batch_y) in enumerate(loader):
            batch_X, batch_y = batch_X.cuda(), batch_y.cuda()
            assert batch_y.ge(0).sum() == batch_y.shape[0], batch_y
            assert batch_y.le(1).sum() == batch_y.shape[0], batch_y
            out = net(batch_X)
            loss = loss_func(out, batch_y)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            if step % 500 == 0:
                print("[T] Step-{}".format(step))

            prediction = torch.max(out, 1)[1]
            pred_y = prediction.data.cpu().numpy().squeeze()
            target_y = batch_y.data.cpu().numpy()
            count += (pred_y == target_y).astype(int).sum()

        accuracy = float(count) / X_train.shape[0]
        print("[T] Epoch-{}: {}%".format(epoch, accuracy*100))

        if epoch % 5 == 0:
            net.eval()
            out = net(X_valid)
            prediction = torch.max(out, 1)[1]
            pred_y = prediction.data.cpu().numpy().squeeze()
            target_y = y_valid.data.cpu().numpy()
            accuracy = float(
                (pred_y == target_y).astype(int).sum()
            ) / float(target_y.size)
            print("[V] Epoch-{}: {}%".format(epoch, accuracy*100))
            if accuracy > best_acc:
                best_acc = accuracy
                torch.save(net.state_dict(), MODEL_NAME)
            else:
                break
            net.train()
