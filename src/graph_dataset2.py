import torch
from torch.utils.data.dataset import Dataset
import scipy.sparse as sp
import numpy as np

from graph2 import Graph


class GraphDataset(Graph, Dataset):

    def __init__(
        self,
        train_edge_filename: str,
        test_edge_filename: str,
        nodes_embedding_filename=None,
        nodes_embedding_type="",
        doc_embedding_filename=None,
        dim=128,
        directed=False
    ):
        super().__init__(
            train_edge_filename,
            test_edge_filename,
            nodes_embedding_filename,
            nodes_embedding_type,
            doc_embedding_filename,
            dim,
            directed
        )
        super().prepare_data()

    def __getitem__(self, index):
        label = self.all_labels[index]
        label = [0, 1] if label == 1 else [1, 0]
        u, v = self.all_edges[index]
        return (
            torch.FloatTensor(self.nodes[u].v).view(-1, 1),
            torch.FloatTensor(self.nodes[v].v).view(-1, 1)
        ), torch.FloatTensor(label)

    def __len__(self):
        return len(self.training_edges)

    def nodes_to_vectors(self):
        vectors = []
        for idx in range(1, self.nodes_num+1):
            if idx in self.nodes:
                v = self.nodes[idx].v
                v = v / np.linalg.norm(v)
            else:
                v = [0] * self.dim
            vectors.append(v)
            if len(v) == 0:
                import pdb
                pdb.set_trace()
                print("")
        return np.array(vectors)

    def load_data_in_pygcn_format(self):
        adj = self.get_adj_matrix()
        adj = sp.csr_matrix(adj)
        # adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
        features = self.nodes_to_vectors()
        features = sp.csr_matrix(features, dtype=np.float32)
        adj = normalize(adj + sp.eye(adj.shape[0]))

        idx_train = []
        idx_val = []
        idx_test = []
        for idx in range(1, self.nodes_num+1):
            if idx in self.nodes_train:
                idx_train.append(idx-1)
            else:
                idx_test.append(idx-1)
        idx_val = idx_train[:len(idx_train)//10]
        idx_train = idx_train[len(idx_train)//10:]
        features = torch.FloatTensor(np.array(features.todense()))
        labels = []
        for node_idx in range(1, self.nodes_num+1):
            labels.append(self.G.degree(node_idx))

        labels = torch.LongTensor(labels)
        adj = sparse_mx_to_torch_sparse_tensor(adj)

        idx_train = torch.LongTensor(idx_train)
        idx_val = torch.LongTensor(idx_val)
        idx_test = torch.LongTensor(idx_test)
        return adj, features, labels, idx_train, idx_val, idx_test


def normalize(mx):
    """Row-normalize sparse matrix"""
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -1).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sp.diags(r_inv)
    mx = r_mat_inv.dot(mx)
    return mx


def sparse_mx_to_torch_sparse_tensor(sparse_mx):
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(
        np.vstack((sparse_mx.row, sparse_mx.col)).astype(np.int64))
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)
