import numpy as np
import networkx as nx

num_nodes = 17500
phase = [-1] * num_nodes  # 1: nodes before t, 2: nodes after t


G = nx.DiGraph()

train = np.loadtxt('data/t2-train.txt', dtype=int)
train = train - np.ones(train.shape, dtype=int)  # to 0-based
test =  np.loadtxt('data/t2-test.txt', dtype=int)
test = test - np.ones(test.shape, dtype=int)  # to 0-based

for u, v in train:
    phase[u] = 1
    phase[v] = 1

for u, v in test:
    if phase[u] == -1: phase[u] = 2
    if phase[v] == -1: phase[v] = 2

edges = np.concatenate((train, test), axis=0)

G.add_nodes_from(edges.reshape(-1))
G.add_edges_from(edges)
condensed_G = nx.condensation(G)
node_sequence = nx.topological_sort(condensed_G)
node_sequence = list(node_sequence)[::-1]
node_sequence_dict = {}
for i, node_id in enumerate(node_sequence):
    node_sequence_dict[node_id] = i
    nodes_to_seq = {}
for n in G.node():
    mapped_id = condensed_G.graph['mapping'][n]
    nodes_to_seq[n] = node_sequence_dict[mapped_id]

max_phase_1 = 0
min_phase_2 = 100000
for i in range(num_nodes):
    if phase[i] == 1 and nodes_to_seq[i] > max_phase_1:
        max_phase_1 = nodes_to_seq[i]
    elif phase[i] == 2 and nodes_to_seq[i] < min_phase_2:
        min_phase_2 = nodes_to_seq[i]

for i in range(num_nodes):
    if phase[i] == 1:
        print(nodes_to_seq[i])
    elif phase[i] == 2:
        print(nodes_to_seq[i] + (max_phase_1 - min_phase_2 + 1))
    elif phase[i] == -1:
        print(100000)
