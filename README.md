# SDML-1 Link Prediction

Still need to fill some random description here.

## Installation
```
git submodule update --init --recursive
conda env create -f envs/<target environment>.yaml
```

## Run
### Train
#### Task 2
`./train.sh task2 [data folder path] [output model params path]`
#### Task 3
Same as above, change `task2` to `task3`
### Eval
#### Task 2
`./eval.sh task2 [data folder path] [model params path]`
The `[model params path]` is the one saved from training part.
#### Task 3
Same as above, change `task2` to `task3`


### Doc2Vec Generation
```
source activate doc2vec
# Task 2
python src/doc_to_line.py -xd data/t2-doc -o data/t2_doc_lines.txt
# Task 3
python src/time_doc_to_line.py -xd data/t3-doc -dof t3_doc_lines.txt -tof t3_time_lines.txt
python src/gensim_doc2vec.py -train data/t2-train.txt -te data/t2-test.txt -dif data/t2_doc_lines.txt -dd t2_doc_embeddings -od t2_outputs -dim 100
```

### PTE Embedding Generation
1. Change data to PTE input format
```
# Just load some dummy embeddings in order to initialize graph2.py
python src/data_to_pte.py -ptrain data/t3_pte_train.txt -ptl data/pte_labels.txt -train data/t3-train.txt -te data/t3-test.txt -n pygcn_dim128_gensim_doc2vec_dim100.embeddings.embeddings -d 128 -df data/t3_doc_lines.txt
```

2. Install [PTE](https://github.com/mnqu/PTE) following its instruction (complie Eigen and GSL, change Eigen path in makefile of each folder, and complie PTE)
```
# Since we fork the PTE module and make it a submodule of this repo, you coulde simply do the following lines after init the submodule:
cd lib/PTE
./download_modules.sh
./make_all.sh
```

3. Change train and label path in run.sh
```
# Since we fork the PTE module and make it a submodule of this repo, you coulde simply do the following lines after init the submodule:
./run_t3.sh
```


### PYGCN training
```
source activate pygcn
python src/train_pygcn2.py -de t2_doc_embeddings/gensim_doc2vec_dim100.embeddings  -train data/t2-train.txt -te data/t2-test.txt --epochs 200 --hidden 128 -dim 100  -nt doc2vec -od t2_embeddings
```

### Link Prediction
```
python src/main.py -train data/t2-train.txt -de t2_doc_embeddings/gensim_doc2vec_dim150.embeddings -te data/t2-test.txt -od t2_outputs -d 150
```

### Eval for cosine similarity
```
source activate STML_HW1
python src/graph2.py -train data/t2-train.txt -te data/t2-test.txt -n t2_embeddings/pygcn_dim128_gensim_doc2vec_dim100.embeddings.embeddings  -od t2_outputs/pygcn128_d2v100  -d 128
```

### Produce POSIX format timestamp
``` bash
python src/convert_datetime_to_unix_timestamp.py -i input_file -o output_file
```

### Train NN
``` bash
CUDA_VISIBLE_DEVICES=1 python src/t3_neural_network/train.py -train data/t3-train.txt -time ./t3_time_lines_posix.txt  -n t3_embeddings/pte100.embeddings -nt pte
```

### Eval NN
``` bash
CUDA_VISIBLE_DEVICES=1 python src/t3_neural_network/eval.py -te data/t3-test.txt -time ./t3_time_lines_posix.txt  -n t3_embeddings/pte100.embeddings -nt pte -m net_params_20181014-115028.pkl > nn_pte100_val0.60.txt
```

## Repo organization
- `data/`: to put data, should be gitignore-d
- `src/`: the main source code
- `model/`: the `.pkl` files for trained model params
- ...

## Working flow
1. `git checkout develop`: always based on `develop` branch
2. `git pull origin develop`: update to latest
3. `git checkout -b feature/xxx`: checkout and create a feature branch
4. `git add this that`
5. `git commit -m 'Add this and that for some reason'`: write meaningful, good commit msg, FYR: https://chris.beams.io/posts/git-commit/
6. GOTO 4. several times
7. `git push origin feature/xxx`: push the local branch to remote repo (in our case, Gitlab)
8. go to "Merge Requests" page and send a PR
9. wait one of the other two team members to merge the PR to `develop` and close the merge issue
